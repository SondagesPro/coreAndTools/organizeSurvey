# organizeSurvey for LimeSurvey

Allow to organize surey with random group with fxed number of question in each group

## Installation

### Via GIT
- Go to your LimeSurvey Directory (version up to 4.4)
- Clone in plugins/organizeSurvey directory

## Usage

You have a new item in tools menu wher youcan organize surveys with random group more easily.

Move questions inside random group, create new question random group, choose the number of question to show in each randome group

## Issues and feature

All issue and merge request must be done in [base repo](https://gitlab.com/SondagesPro/coreAndTools/organizeSurvey) (currently gitlab).

Issue must be posted with complete information : LimeSurvey version and build number, web server version, PHP version, SQL type and version number … 
**Reminder:** no warranty of functionnal system operation is offered on this software. If you want a professional offer: please [contact Sondages Pro](https://extensions.sondages.pro/about/contact.html).

## Home page & Copyright
- HomePage <http://extensions.sondages.pro/>
- Copyright © 2019-2024 Denis Chenu <http://sondages.pro>
- Copyright © 2019-2020 UNIL | Université de Lausanne - Suisse <https://unil.ch/>
- Licence : GNU Affero General Public License <https://www.gnu.org/licenses/agpl-3.0.html>
- [Donate](https://support.sondages.pro/open.php?topicId=12), [Liberapay](https://liberapay.com/SondagesPro/), [OpenCollective](https://opencollective.com/sondagespro) 

## Support
- Issues <https://gitlab.com/SondagesPro/mailing/sendMailCron/issues>
- Professional support <https://support.sondages.pro/>
